<?php

// +----------------------------------------------------------------------
// | 企业微信群通知
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 [ https://www.dtapp.net ]
// +----------------------------------------------------------------------
// | 官方网站: https://gitee.com/liguangchun/qyweixin-grouprobot
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 仓库地址 ：https://gitee.com/liguangchun/qyweixin-grouprobot.git
// | github 仓库地址 ：https://github.com/GC0202/qyweixin-grouprobot.git
// | huaweicloud 仓库地址：https://codehub-cn-south-1.devcloud.huaweicloud.com/composer00001/qyweixin-grouprobot.git
// | weixin 仓库地址：https://git.weixin.qq.com/liguangchun/qyweixin-grouprobot.git
// | gitlab 仓库地址：https://gitlab.com/liguangchun/qyweixin-grouprobot.git
// | aliyun 仓库地址：https://code.aliyun.com/liguancghun/qyweixin-grouprobot.git
// | tencent 仓库地址：https://e.coding.net/liguangchundt/qyweixin-grouprobot.git
// | Packagist 地址 ：https://packagist.org/packages/liguangchun/qyweixin-grouprobot
// +----------------------------------------------------------------------

namespace DtApp\Notice\QyWeiXin;

use ArrayAccess;

/**
 * 数据
 * Class DataArray
 * @package DtApp\Notice\DingTalk
 */
class DataArray implements ArrayAccess
{
    /**
     * 当前配置值
     * @var array
     */
    private $config = [];

    /**
     * DataArray constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->config = $options;
    }

    /**
     * 设置配置项值
     * @param $offset
     * @param $value
     */
    public function set($offset, $value)
    {
        $this->offsetSet($offset, $value);
    }

    /**
     * 获取配置项参数
     * @param null $offset
     * @return mixed|void
     */
    public function get($offset = null)
    {
        return $this->offsetGet($offset);
    }

    /**
     * 合并数据到对象
     * @param array $data 需要合并的数据
     * @param bool $append 是否追加数据
     * @return array
     */
    public function merge(array $data, $append = false)
    {
        if ($append) return $this->config = array_merge($this->config, $data);
        return array_merge($this->config, $data);
    }

    /**
     * 设置配置项值
     * @param string $offset
     * @param string|array|null|integer $value
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) $this->config[] = $value;
        $this->config[$offset] = $value;
    }

    /**
     * 判断配置Key是否存在
     * @param string $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->config[$offset]);
    }

    /**
     * 清理配置项
     * @param string|null $offset
     */
    public function offsetUnset($offset = null)
    {
        if (is_null($offset)) $this->config = [];
        unset($this->config[$offset]);
    }

    /**
     * 获取配置项参数
     * @param string|null $offset
     * @return array|string|null
     */
    public function offsetGet($offset = null)
    {
        if (is_null($offset)) return $this->config;
        return isset($this->config[$offset]) ? $this->config[$offset] : null;
    }
}
